import pulumi
import pulumi_kubernetes.helm.v3 as helm
from typing import Optional


class IntelDevicePlugins(pulumi.ComponentResource):
    def __init__(
        self,
        name: str,
        opts: Optional[pulumi.ResourceOptions] = None,
    ):
        super().__init__("kubernetes:intel:DevicePlugins", name, None, opts)

        node_feature_discovery = helm.Release(
            "node-feature-discovery",
            helm.ReleaseArgs(
                name="node-feature-discovery",
                chart="node-feature-discovery",
                repository_opts=helm.RepositoryOptsArgs(
                    repo="https://kubernetes-sigs.github.io/node-feature-discovery/charts"
                ),
                namespace="node-feature-discovery",
                create_namespace=True,
            ),
            opts=pulumi.ResourceOptions(parent=self),
        )

        helm_repository = "https://intel.github.io/helm-charts/"

        operator = helm.Release(
            "operator",
            helm.ReleaseArgs(
                name="intel-device-plugins-operator",
                chart="intel-device-plugins-operator",
                repository_opts=helm.RepositoryOptsArgs(repo=helm_repository),
                namespace="inteldeviceplugins-system",
                create_namespace=True,
            ),
            opts=pulumi.ResourceOptions(
                parent=self, depends_on=[node_feature_discovery]
            ),
        )

        helm.Release(
            "gpu-plugin",
            helm.ReleaseArgs(
                name="intel-device-plugins-gpu",
                chart="intel-device-plugins-gpu",
                repository_opts=helm.RepositoryOptsArgs(repo=helm_repository),
                namespace="inteldeviceplugins-system",
                create_namespace=True,
                values={
                    "name": "gpudeviceplugin",
                    "sharedDevNum": 3,
                },
            ),
            opts=pulumi.ResourceOptions(parent=self, depends_on=[operator]),
        )
