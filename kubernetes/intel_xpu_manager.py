import pulumi

from typing import Optional

import pulumi_kubernetes as kubernetes


class IntelXpuManager(pulumi.ComponentResource):
    def __init__(
        self,
        name: str,
        opts: Optional[pulumi.ResourceOptions] = None,
    ):
        super().__init__("kubernetes:intel:XpuManager", name, None, opts)

        namespace = kubernetes.core.v1.Namespace(
            "namespace",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(name="intel-xpu-manager"),
            opts=pulumi.ResourceOptions(parent=self),
        )

        daemonset = kubernetes.apps.v1.DaemonSet(
            "intel-xpumanager",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                labels={"app": "intel-xpumanager"},
                name="intel-xpumanager",
                namespace="intel-xpu-manager",
            ),
            spec=kubernetes.apps.v1.DaemonSetSpecArgs(
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels={"app": "intel-xpumanager"}
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=kubernetes.meta.v1.ObjectMetaArgs(
                        labels={"app": "intel-xpumanager"}
                    ),
                    spec=kubernetes.core.v1.PodSpecArgs(
                        host_network=True,
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name="xpumd",
                                image="intel/xpumanager",
                                image_pull_policy="IfNotPresent",
                                command=["/usr/bin/xpumd"],
                                env=[
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="SPDLOG_LEVEL", value="info"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="XPUM_REST_NO_TLS", value="1"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="XPUM_EXPORTER_NO_AUTH", value="1"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="XPUM_EXPORTER_ONLY", value="1"
                                    ),
                                ],
                                resources=kubernetes.core.v1.ResourceRequirementsArgs(
                                    limits={"gpu.intel.com/i915_monitoring": "1"}
                                ),
                                security_context=kubernetes.core.v1.SecurityContextArgs(
                                    privileged=False,
                                    read_only_root_filesystem=True,
                                    allow_privilege_escalation=False,
                                    run_as_user=0,
                                    capabilities=kubernetes.core.v1.CapabilitiesArgs(
                                        drop=["ALL"], add=["SYS_ADMIN"]
                                    ),
                                ),
                                volume_mounts=[
                                    kubernetes.core.v1.VolumeMountArgs(
                                        mount_path="/var/lib/kubelet/pod-resources",
                                        name="kubeletpodres",
                                    ),
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name="sockdir", mount_path="/tmp"
                                    ),
                                ],
                            ),
                            kubernetes.core.v1.ContainerArgs(
                                name="python-exporter",
                                image="intel/xpumanager",
                                image_pull_policy="IfNotPresent",
                                volume_mounts=[
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name="sockdir", mount_path="/tmp"
                                    ),
                                    kubernetes.core.v1.VolumeMountArgs(
                                        name="devdri",
                                        mount_path="/dev/dri",
                                        read_only=True,
                                    ),
                                ],
                                security_context=kubernetes.core.v1.SecurityContextArgs(
                                    allow_privilege_escalation=False,
                                    read_only_root_filesystem=True,
                                    run_as_user=0,
                                    capabilities=kubernetes.core.v1.CapabilitiesArgs(
                                        drop=["ALL"]
                                    ),
                                ),
                                env=[
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="NODE_NAME",
                                        value_from=kubernetes.core.v1.EnvVarSourceArgs(
                                            field_ref=kubernetes.core.v1.ObjectFieldSelectorArgs(
                                                api_version="v1",
                                                field_path="spec.nodeName",
                                            )
                                        ),
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="XPUM_EXPORTER_NO_AUTH", value="1"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="XPUM_EXPORTER_ONLY", value="1"
                                    ),
                                ],
                                working_dir="/usr/lib/xpum/rest",
                                command=[
                                    "gunicorn",
                                    "--bind",
                                    "0.0.0.0:29999",
                                    "--worker-connections",
                                    "64",
                                    "--worker-class",
                                    "gthread",
                                    "--workers",
                                    "1",
                                    "--threads",
                                    "4",
                                    "xpum_rest_main:main()",
                                ],
                                ports=[
                                    kubernetes.core.v1.ContainerPortArgs(
                                        container_port=29999,
                                        name="metrics",
                                        protocol="TCP",
                                    )
                                ],
                                startup_probe=kubernetes.core.v1.ProbeArgs(
                                    http_get=kubernetes.core.v1.HTTPGetActionArgs(
                                        path="/metrics", port="metrics"
                                    ),
                                    failure_threshold=10,
                                    period_seconds=10,
                                ),
                                liveness_probe=kubernetes.core.v1.ProbeArgs(
                                    http_get=kubernetes.core.v1.HTTPGetActionArgs(
                                        path="/healtz", port="metrics"
                                    ),
                                    initial_delay_seconds=60,
                                    period_seconds=10,
                                ),
                            ),
                        ],
                        node_selector={"kubernetes.io/arch": "amd64"},
                        restart_policy="Always",
                        volumes=[
                            kubernetes.core.v1.VolumeArgs(
                                name="kubeletpodres",
                                host_path=kubernetes.core.v1.HostPathVolumeSourceArgs(
                                    path="/var/lib/kubelet/pod-resources", type=""
                                ),
                            ),
                            kubernetes.core.v1.VolumeArgs(
                                name="sockdir",
                                empty_dir=kubernetes.core.v1.EmptyDirVolumeSourceArgs(
                                    medium="Memory"
                                ),
                            ),
                            kubernetes.core.v1.VolumeArgs(
                                name="devdri",
                                host_path=kubernetes.core.v1.HostPathVolumeSourceArgs(
                                    path="/dev/dri"
                                ),
                            ),
                        ],
                    ),
                ),
            ),
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[namespace],
                delete_before_replace=True,
            ),
        )

        service = kubernetes.core.v1.Service(
            "service",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="intel-xpumanager",
                namespace="intel-xpu-manager",
                labels={"app": "intel-xpumanager"},
            ),
            spec=kubernetes.core.v1.ServiceSpecArgs(
                ports=[
                    kubernetes.core.v1.ServicePortArgs(
                        name="metrics",
                        port=29999,
                        protocol="TCP",
                    )
                ],
                selector={"app": "intel-xpumanager"},
                session_affinity="None",
            ),
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[daemonset],
                delete_before_replace=True,
            ),
        )

        kubernetes.apiextensions.CustomResource(
            "service-monitor",
            api_version="monitoring.coreos.com/v1",
            kind="ServiceMonitor",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="intel-xpumanager",
                namespace="intel-xpu-manager",
                labels={"app": "intel-xpumanager"},
            ),
            spec={
                "selector": {"matchLabels": {"app": "intel-xpumanager"}},
                "namespaceSelector": {"matchNames": ["intel-xpu-manager"]},
                "endpoints": [
                    {
                        "port": "metrics",
                        "path": "/metrics",
                        "interval": "5s",
                        "scrapeTimeout": "4s",
                    }
                ],
            },
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[service],
                delete_before_replace=True,
            ),
        )
