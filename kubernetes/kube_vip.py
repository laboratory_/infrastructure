import pulumi

from typing import Optional

import pulumi_kubernetes as kubernetes


class KubeVip(pulumi.ComponentResource):
    def __init__(
        self,
        name: str,
        interface: str,
        address: str,
        opts: Optional[pulumi.ResourceOptions] = None,
    ):
        super().__init__("kubernetes:cni:KubeVip", name, None, opts)

        rbac = kubernetes.yaml.v2.ConfigGroup(
            "rbac",
            files=["https://kube-vip.io/manifests/rbac.yaml"],
            opts=pulumi.ResourceOptions(parent=self),
        )

        kubernetes.apps.v1.DaemonSet(
            "kube-vip",
            metadata={
                "name": "kube-vip",
                "namespace": "kube-system",
            },
            spec=kubernetes.apps.v1.DaemonSetSpecArgs(
                selector=kubernetes.meta.v1.LabelSelectorArgs(
                    match_labels={"name": "kube-vip"},
                ),
                template=kubernetes.core.v1.PodTemplateSpecArgs(
                    metadata=kubernetes.meta.v1.ObjectMetaArgs(
                        labels={"name": "kube-vip"},
                    ),
                    spec=kubernetes.core.v1.PodSpecArgs(
                        affinity=kubernetes.core.v1.AffinityArgs(
                            node_affinity=kubernetes.core.v1.NodeAffinityArgs(
                                required_during_scheduling_ignored_during_execution=kubernetes.core.v1.NodeSelectorArgs(
                                    node_selector_terms=[
                                        kubernetes.core.v1.NodeSelectorTermArgs(
                                            match_expressions=[
                                                kubernetes.core.v1.NodeSelectorRequirementArgs(
                                                    key="node-role.kubernetes.io/master",
                                                    operator="Exists",
                                                ),
                                                kubernetes.core.v1.NodeSelectorRequirementArgs(
                                                    key="node-role.kubernetes.io/control-plane",
                                                    operator="Exists",
                                                ),
                                            ]
                                        ),
                                    ],
                                ),
                            ),
                        ),
                        containers=[
                            kubernetes.core.v1.ContainerArgs(
                                name="kube-vip",
                                image="ghcr.io/kube-vip/kube-vip:v0.8.3",
                                image_pull_policy="Always",
                                args=["manager"],
                                env=[
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_arp", value="true"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="port", value="6443"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_interface", value=interface
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_cidr", value="32"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="cp_enable", value="true"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="cp_namespace", value="kube-system"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_ddns", value="false"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="svc_enable", value="true"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_leaderelection", value="true"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_leaseduration", value="5"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_renewdeadline", value="3"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="vip_retryperiod", value="1"
                                    ),
                                    kubernetes.core.v1.EnvVarArgs(
                                        name="address", value=address
                                    ),
                                ],
                                security_context=kubernetes.core.v1.SecurityContextArgs(
                                    capabilities=kubernetes.core.v1.CapabilitiesArgs(
                                        add=["NET_ADMIN", "NET_RAW", "SYS_TIME"],
                                    ),
                                ),
                            ),
                        ],
                        host_network=True,
                        service_account_name="kube-vip",
                        tolerations=[
                            kubernetes.core.v1.TolerationArgs(
                                effect="NoSchedule",
                                operator="Exists",
                            ),
                            kubernetes.core.v1.TolerationArgs(
                                effect="NoExecute",
                                operator="Exists",
                            ),
                        ],
                    ),
                ),
                update_strategy=kubernetes.apps.v1.DaemonSetUpdateStrategyArgs(
                    type="RollingUpdate"
                ),
            ),
            opts=pulumi.ResourceOptions(parent=self, depends_on=[rbac]),
        )
