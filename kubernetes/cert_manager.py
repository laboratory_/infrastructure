import pulumi
from typing import Optional
import pulumi_cloudflare as cloudflare
import pulumi_kubernetes as kubernetes
import pulumi_kubernetes.helm.v3 as helm


class CertManager(pulumi.ComponentResource):
    def __init__(
        self,
        name: str,
        opts: Optional[pulumi.ResourceOptions] = None,
    ):
        super().__init__("kubernetes:controller:cert-manager", name, None, opts)

        zone_id = "ecca2c1ddddb60285b67581052311d1b"

        permission_groups = cloudflare.get_api_token_permission_groups()

        api_token = cloudflare.ApiToken(
            "api-token",
            name="cert-manager",
            policies=[
                cloudflare.ApiTokenPolicyArgs(
                    resources={f"com.cloudflare.api.account.zone.{zone_id}": "*"},
                    permission_groups=[
                        permission_groups.zone["DNS Write"],
                        permission_groups.zone["DNS Read"],
                    ],
                )
            ],
        )

        cert_manager = helm.Release(
            "this",
            helm.ReleaseArgs(
                name="cert-manager",
                chart="cert-manager",
                version="1.15.1",
                repository_opts=helm.RepositoryOptsArgs(
                    repo="https://charts.jetstack.io",
                ),
                namespace="cert-manager",
                create_namespace=True,
                values={
                    "crds": {"enabled": True},
                    "featureGates": "ServerSideApply=true",
                },
                wait_for_jobs=True,
                atomic=True,
            ),
            opts=pulumi.ResourceOptions(parent=self),
        )

        cloudflare_api_token_secret = kubernetes.core.v1.Secret(
            "cloudflare-api-token-secret",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="cloudflare-api-token", namespace="cert-manager"
            ),
            string_data={"api-token": api_token.value},
            opts=pulumi.ResourceOptions(parent=self, depends_on=[cert_manager]),
        )

        kubernetes.apiextensions.CustomResource(
            "cluster-issuer-letsencrypt-staging",
            api_version="cert-manager.io/v1",
            kind="ClusterIssuer",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(name="letsencrypt-staging"),
            spec={
                "acme": {
                    "server": "https://acme-staging-v02.api.letsencrypt.org/directory",
                    "email": "isculats@protonmail.com",
                    "privateKeySecretRef": {"name": "letsencrypt-staging"},
                    "solvers": [
                        {
                            "dns01": {
                                "cloudflare": {
                                    "apiTokenSecretRef": {
                                        "name": "cloudflare-api-token",
                                        "key": "api-token",
                                    }
                                }
                            }
                        }
                    ],
                }
            },
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[cert_manager, cloudflare_api_token_secret],
                aliases=[
                    "urn:pulumi:production::laboratory-kubernetes::kubernetes:cert-manager.io/v1:ClusterIssuer::cert-manager-cluster-issuer-letsencrypt-staging"
                ],
            ),
        )

        kubernetes.apiextensions.CustomResource(
            "cluster-issuer-letsencrypt-production",
            api_version="cert-manager.io/v1",
            kind="ClusterIssuer",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(name="letsencrypt-production"),
            spec={
                "acme": {
                    "server": "https://acme-v02.api.letsencrypt.org/directory",
                    "email": "isculats@protonmail.com",
                    "privateKeySecretRef": {"name": "letsencrypt-production"},
                    "solvers": [
                        {
                            "dns01": {
                                "cloudflare": {
                                    "apiTokenSecretRef": {
                                        "name": "cloudflare-api-token",
                                        "key": "api-token",
                                    }
                                }
                            }
                        }
                    ],
                }
            },
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[cert_manager, cloudflare_api_token_secret],
            ),
        )
