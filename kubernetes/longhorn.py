import pulumi
import pulumi_cloudflare as cloudflare
import pulumi_kubernetes as kubernetes
import pulumi_kubernetes.kustomize as kustomize
import pulumi_kubernetes.helm.v3 as helm
from typing import Optional
import hashlib


class Longhorn(pulumi.ComponentResource):
    def __init__(
        self,
        name: str,
        opts: Optional[pulumi.ResourceOptions] = None,
    ):
        super().__init__("kubernetes:csi:Longhorn", name, None, opts)

        snapshot_crds = kustomize.Directory(
            "snapshot-crds",
            directory="https://github.com/kubernetes-csi/external-snapshotter/tree/v6.3.2/client/config/crd",
            opts=pulumi.ResourceOptions(parent=self),
        )

        snapshot_controller = kustomize.Directory(
            "snapshot-controller",
            directory="https://github.com/kubernetes-csi/external-snapshotter/tree/v6.3.2/deploy/kubernetes/snapshot-controller",
            opts=pulumi.ResourceOptions(parent=self, depends_on=[snapshot_crds]),
        )

        acount_id = "2f22728d4135e0f22c10e088913349d6"

        bucket_name = "laboratory-production-longhorn"

        bucket = cloudflare.R2Bucket(
            "backup",
            account_id=acount_id,
            name=bucket_name,
            location="EEUR",
            opts=pulumi.ResourceOptions(parent=self),
        )

        permission_groups = cloudflare.get_api_token_permission_groups()

        api_token = cloudflare.ApiToken(
            "api-token",
            name="longhorn",
            policies=[
                cloudflare.ApiTokenPolicyArgs(
                    resources={
                        f"com.cloudflare.edge.r2.bucket.{acount_id}_default_{bucket_name}": "*"
                    },
                    permission_groups=[
                        permission_groups.r2["Workers R2 Storage Bucket Item Write"],
                        permission_groups.r2["Workers R2 Storage Bucket Item Read"],
                    ],
                )
            ],
            opts=pulumi.ResourceOptions(parent=self, depends_on=[bucket]),
        )

        namespace = kubernetes.core.v1.Namespace(
            "namespace",
            metadata={"name": "longhorn-system"},
            opts=pulumi.ResourceOptions(parent=self),
        )

        cloudflare_r2_secret = kubernetes.core.v1.Secret(
            "cloudflare-r2-secret",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="cloudflare-r2-secret",
                namespace="longhorn-system",
            ),
            string_data={
                "AWS_ENDPOINTS": f"https://{acount_id}.r2.cloudflarestorage.com",
                "AWS_ACCESS_KEY_ID": api_token.id,
                "AWS_SECRET_ACCESS_KEY": api_token.value.apply(
                    lambda value: hashlib.sha256(value.encode()).hexdigest()
                ),
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[api_token, namespace]),
        )

        helm_release = helm.Release(
            "longhorn-helm-release",
            helm.ReleaseArgs(
                name="longhorn",
                chart="longhorn",
                version="1.7.1",
                repository_opts=helm.RepositoryOptsArgs(
                    repo="https://charts.longhorn.io",
                ),
                namespace="longhorn-system",
                values={
                    "persistence": {
                        "defaultFsType": "xfs",
                        "defaultClassReplicaCount": 1,
                        "defaultDataLocality": "best-effort",
                    },
                    "defaultSettings": {
                        "backupTarget": bucket.name.apply(
                            lambda name: f"s3://{name}@auto/"
                        ),
                        "backupTargetCredentialSecret": "cloudflare-r2-secret",
                        "allowCollectingLonghornUsageMetrics": False,
                        "defaultDataLocality": "best-effort",
                        "defaultReplicaCount": 1,
                        "logLevel": "Warn",
                    },
                },
                wait_for_jobs=True,
                atomic=True,
            ),
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[
                    namespace,
                    snapshot_controller,
                    cloudflare_r2_secret,
                ],
            ),
        )

        local_storage_class = kubernetes.storage.v1.StorageClass(
            "local-storage-class",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(name="longhorn-local"),
            provisioner="driver.longhorn.io",
            allow_volume_expansion=True,
            reclaim_policy="Delete",
            mount_options=["noatime"],
            parameters={
                "numberOfReplicas": "1",
                "fsType": "xfs",
                "dataLocality": "strict-local",
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[helm_release]),
        )

        kubernetes.yaml.v2.ConfigGroup(
            "volume-snapshot-class",
            objs=[
                {
                    "kind": "VolumeSnapshotClass",
                    "apiVersion": "snapshot.storage.k8s.io/v1",
                    "metadata": {"name": "longhorn"},
                    "driver": "driver.longhorn.io",
                    "deletionPolicy": "Delete",
                }
            ],
            opts=pulumi.ResourceOptions(
                parent=self,
                depends_on=[snapshot_controller, helm_release],
            ),
        )
