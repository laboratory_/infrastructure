import pulumi
import pulumi_kubernetes.helm.v3 as helm

from cilium import Cilium
from kube_vip import KubeVip

from longhorn import Longhorn

from cert_manager import CertManager

from intel_device_plugins import IntelDevicePlugins
from intel_xpu_manager import IntelXpuManager

cilium = Cilium("cilium")

kube_vip = KubeVip(
    "kube-vip",
    interface="enp4s0",
    address="10.0.2.2",
    opts=pulumi.ResourceOptions(depends_on=[cilium]),
)

longhorn = Longhorn("longhorn", opts=pulumi.ResourceOptions(depends_on=[cilium]))

cert_manager = CertManager(
    "cert-manager", opts=pulumi.ResourceOptions(depends_on=[cilium])
)

intel_device_plugins = IntelDevicePlugins(
    "intel-device-plugins", opts=pulumi.ResourceOptions(depends_on=[cert_manager])
)

intel_xpu_manager = IntelXpuManager(
    "intel-xpu-manager",
    opts=pulumi.ResourceOptions(depends_on=[intel_device_plugins]),
)

config = pulumi.Config()

prometheus = config.require_object("prometheus")
loki = config.require_object("loki")

k8s_monitoring = helm.Release(
    "k8s-monitoring",
    helm.ReleaseArgs(
        name="k8s-monitoring",
        version="1.4.4",
        chart="k8s-monitoring",
        repository_opts=helm.RepositoryOptsArgs(
            repo="https://grafana.github.io/helm-charts",
        ),
        namespace="k8s-monitoring",
        create_namespace=True,
        values={
            "cluster": {"name": "laboratory"},
            "externalServices": {
                "prometheus": {
                    "host": prometheus["host"],
                    "basicAuth": {
                        "username": prometheus["username"],
                        "password": prometheus["password"],
                    },
                },
                "loki": {
                    "host": loki["host"],
                    "basicAuth": {
                        "username": loki["username"],
                        "password": loki["password"],
                    },
                },
            },
            "metrics": {
                "enabled": True,
                "cost": {"enabled": False},
                "node-exporter": {
                    "enabled": True,
                    "metricsTuning": {
                        "includeMetrics": [
                            "node_hwmon_temp_celsius",
                            "node_hwmon_temp_crit_celsius",
                            "node_hwmon_sensor_label",
                            "node_hwmon_chip_names",
                        ]
                    },
                },
            },
            "logs": {
                "enabled": True,
                "pod_logs": {"enabled": True},
                "cluster_events": {"enabled": True},
            },
            "traces": {"enabled": False},
            "receivers": {
                "grpc": {"enabled": False},
                "http": {"enabled": False},
                "zipkin": {"enabled": False},
                "grafanaCloudMetrics": {"enabled": False},
            },
            "opencost": {"enabled": False},
            "kube-state-metrics": {"enabled": True},
            "prometheus-node-exporter": {"enabled": True},
            "prometheus-operator-crds": {"enabled": True},
            "alloy": {},
            "alloy-events": {},
            "alloy-logs": {},
        },
        wait_for_jobs=True,
        atomic=True,
    ),
    opts=pulumi.ResourceOptions(depends_on=[cilium]),
)

metrics_server = helm.Release(
    "metrics-server",
    helm.ReleaseArgs(
        name="metrics-server",
        chart="metrics-server",
        version="3.12.1",
        repository_opts=helm.RepositoryOptsArgs(
            repo="https://kubernetes-sigs.github.io/metrics-server",
        ),
        namespace="metrics-server",
        create_namespace=True,
        wait_for_jobs=True,
        atomic=True,
    ),
    opts=pulumi.ResourceOptions(depends_on=[cilium]),
)
