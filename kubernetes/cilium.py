import pulumi
import pulumi_kubernetes as kubernetes
import pulumi_kubernetes.helm.v3 as helm
from typing import Optional


class Cilium(pulumi.ComponentResource):
    def __init__(self, name: str, opts: Optional[pulumi.ResourceOptions] = None):
        super().__init__("kubernetes:cni:Cilium", name, None, opts)

        helm_release = helm.Release(
            "cilium",
            helm.ReleaseArgs(
                name="cilium",
                chart="cilium",
                version="1.16.1",
                repository_opts=helm.RepositoryOptsArgs(
                    repo="https://helm.cilium.io",
                ),
                namespace="kube-system",
                values={
                    "rollOutCiliumPods": True,
                    "ipam": {
                        "operator": {"clusterPoolIPv4PodCIDRList": "10.42.0.0/16"}
                    },
                    "kubeProxyReplacement": True,
                    "k8sServiceHost": "10.0.2.3",
                    "k8sServicePort": 6443,
                    "bgpControlPlane": {"enabled": True},
                    "routingMode": "native",
                    "ipv4NativeRoutingCIDR": "10.42.0.0/16",
                    "autoDirectNodeRoutes": True,
                    "bpf": {"datapathMode": "netkit", "masquerade": True},
                    "enableIPv4BIGTCP": True,
                    "bandwidthManager": {"enabled": True, "bbr": True},
                    "loadBalancer": {"mode": "dsr"},
                    "ingressController": {
                        "enabled": True,
                        "loadbalancerMode": "shared",
                        "default": True,
                    },
                    "operator": {"rollOutPods": True},
                    "hubble": {"enabled": False},
                },
                skip_await=True,
            ),
            opts=pulumi.ResourceOptions(parent=self),
        )

        cilium_api_version = "cilium.io/v2alpha1"

        bgp_advertisement = kubernetes.apiextensions.CustomResource(
            "bgp-advertisement",
            api_version=cilium_api_version,
            kind="CiliumBGPAdvertisement",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="default",
                namespace="kube-system",
                labels={"advertise": "bgp"},
            ),
            spec={
                "advertisements": [
                    {
                        "advertisementType": "Service",
                        "service": {
                            "addresses": [
                                "ExternalIP",
                                "LoadBalancerIP",
                            ]
                        },
                        "selector": {
                            "matchExpressions": [
                                {
                                    "key": "never-used-key",
                                    "operator": "NotIn",
                                    "values": ["never-used-value"],
                                }
                            ]
                        },
                    },
                ]
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[helm_release]),
        )

        bgp_peer_config = kubernetes.apiextensions.CustomResource(
            "bgp-peer-config",
            api_version=cilium_api_version,
            kind="CiliumBGPPeerConfig",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="default",
                namespace="kube-system",
            ),
            spec={
                "gracefulRestart": {
                    "enabled": True,
                    "restartTimeSeconds": 15,
                },
                "families": [
                    {
                        "afi": "ipv4",
                        "safi": "unicast",
                        "advertisements": {"matchLabels": {"advertise": "bgp"}},
                    }
                ],
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[bgp_advertisement]),
        )

        bgp_cluster_config = kubernetes.apiextensions.CustomResource(
            "bgp-cluster-config",
            api_version=cilium_api_version,
            kind="CiliumBGPClusterConfig",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="default",
                namespace="kube-system",
            ),
            spec={
                "nodeSelector": {
                    "matchLabels": {"node-role.kubernetes.io/control-plane": "true"}
                },
                "bgpInstances": [
                    {
                        "name": "default",
                        "localASN": 64512,
                        "peers": [
                            {
                                "name": "router",
                                "peerASN": 64512,
                                "peerAddress": "10.0.0.1",
                                "peerConfigRef": {"name": "default"},
                            }
                        ],
                    }
                ],
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[bgp_peer_config]),
        )

        kubernetes.apiextensions.CustomResource(
            "loadbalancer-ip-pool",
            api_version=cilium_api_version,
            kind="CiliumLoadBalancerIPPool",
            metadata=kubernetes.meta.v1.ObjectMetaArgs(
                name="default",
                namespace="kube-system",
            ),
            spec={
                "allowFirstLastIPs": "No",
                "blocks": [{"cidr": "10.1.0.0/23"}],
            },
            opts=pulumi.ResourceOptions(parent=self, depends_on=[bgp_cluster_config]),
        )
